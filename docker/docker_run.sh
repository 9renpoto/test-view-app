#!/bin/sh

set -e

PATH=/home/app/local/python-2.7.9/bin:$PATH

git fetch
git checkout $GIT_BRANCH
git pull

pip install -r requirements.txt

python myapp.py
