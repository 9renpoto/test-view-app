# -*- coding: utf-8 -*-
import unittest
import myapp
import json
import os


class appTest(unittest.TestCase):
    TEST_SUBDOMAIN_NAME = "test_subdomain"
    CONTENT = '    <script src="//getbootstrap.com/2.3.2/assets/js/jquery.js"></script>\n'
    MIRAGE_LIST_RESPONSE = """
    {
      "result": [
        {
          "id": "47b2ea82e5247a3ae2ffff92f43c20cb0c7586578a5b51411299c595818cac92",
          "short_id": "47b2ea82e524",
          "subdomain": "fuga",
          "branch": "master",
          "image": "myapp:latest",
          "ipaddress": "172.17.0.3"
        }
      ]
    }
"""

    def setUp(self):
        self.app = myapp.app.test_client()
        self.mirage = myapp.MirageAccessor('http')

    def test_form(self):
        res = self.app.get('/form', follow_redirects=False)
        self.assertEqual('200 OK', res.status)

    def test_post(self):
        res = self.app.post('/add', data=dict(
            subdomain=self.TEST_SUBDOMAIN_NAME,
            head_content=self.CONTENT,
        ), follow_redirects=True)

        self.assertEqual('200 OK', res.status)

    def test_create_new_branch(self):
        myapp.create_new_branch(self.TEST_SUBDOMAIN_NAME, self.CONTENT)

    def test_create_new_template(self):
        myapp.create_new_template(self.CONTENT)

        base_dir = os.path.abspath(os.path.dirname(__file__))
        template_dir = os.path.join(base_dir, 'templates')

        with open(template_dir + '/index.html', "r") as f:
            next(line for line in f.readlines() if line == self.CONTENT)

    def test_mirage_accessor(self):
        self.assertEqual(self.mirage.MIRAGE_API_URL, "http://docker.192.168.33.33.xip.io")

    def test_list(self):
        json.loads(self.mirage.list())

    def test_launch(self):
        res = self.mirage.launch(
            "subdmain", "branch", "myapp:latest"
        )
        self.assertEqual(res, '{"result":"ok"}')

    def test_active_subdomain_list(self):
        json_data = json.loads(self.MIRAGE_LIST_RESPONSE)
        res = self.mirage.active_subdomain_list(self.MIRAGE_LIST_RESPONSE)

        self.assertIn(json_data['result'][0]['subdomain'], res)

    def tearDown(self):
        # TODO remove test branch
        pass


if __name__ == '__main__':
    unittest.main()
