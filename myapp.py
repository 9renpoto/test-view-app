# -*- coding: utf-8 -*-
from flask import Flask, render_template, request, redirect, url_for
from git import Repo
from urlparse import urljoin
from urllib import urlopen, urlencode
import os
import json
from time import strftime

app = Flask(__name__)

REVERSE_PROXY_SUFFIX = ".192.168.33.33.xip.io"
docker_image = "myapp:latest"


@app.errorhandler(500)
def page_500(error):
    return 'internal server error', 500


@app.errorhandler(404)
def page_404(error):
    return '404 not found', 404


@app.route('/')
def content():
    http_mirage = MirageAccessor('http')
    # https_mirage = MirageAccessor('https')
    urls = http_mirage.active_url_list()
    # urls.extend(https_mirage.active_url_list())
    return render_template('index.html', active_urls=urls)


@app.route('/form')
def form():
    mirage = MirageAccessor('http')
    # https_mirage = MirageAccessor('https')
    urls = mirage.active_url_list()
    # urls.extend(https_mirage.active_url_list())
    return render_template('form.html', active_urls=urls)


@app.route('/add', methods=['POST'])
def add_content():
    protocol = request.form['protocol']
    subdomain = request.form['subdomain']
    branch_name = 'myapp/' + subdomain + strftime("-%Y%m%d-%H%M%S")
    body_content = request.form['body_content']
    head_content = request.form['head_content']
    if len(subdomain) < 1:
        return redirect(url_for('page_500'))

    create_new_branch(branch_name, head_content, body_content)
    mirage = MirageAccessor(protocol)
    mirage.launch(subdomain, branch_name, docker_image)

    return redirect(url_for('content'))


@app.route('/hello')
def hello():
    return 'world'


def create_new_branch(new_branch_name, head_content='', body_content=''):
    # TODO validate
    base_dir = os.path.dirname(__file__)
    repo = Repo(base_dir)
    now_branch = repo.active_branch

    git = repo.git
    try:
        git.reset("--hard", "HEAD")
        git.checkout("master")
        git.pull("origin", "master")
        git.checkout(b=new_branch_name)

        create_new_template(head_content, body_content)

        git.commit(os.path.join(base_dir, 'templates/index.html'), message='autocommit')
        git.push('origin', new_branch_name)
    finally:
        git.checkout(now_branch)


def create_new_template(head_content, body_content=''):
    base_dir = os.path.abspath(os.path.dirname(__file__))
    template_dir = os.path.join(base_dir, 'templates')

    with open(os.path.join(template_dir, 'index.html'), "w") as fw:
        fw.writelines('{% extends "layout.html" %}\n')
        fw.writelines('{% block head_end %}\n')
        for line in head_content.splitlines():
            fw.writelines(line + '\n')
        fw.writelines('{% endblock %}\n')
        fw.writelines('{% block body_end %}\n')
        for line in body_content.splitlines():
            fw.writelines(line + '\n')
        fw.writelines('<footer><p>auto generated templates</p></footer>\n')
        fw.writelines('{% endblock %}\n')


class MirageAccessor():
    REVERSE_PROXY_SUFFIX = ".192.168.33.33.xip.io"

    def __init__(self, protocol, ):
        self.PROTOCOL = protocol

        mirage_prefix = 'docker'
        if self.PROTOCOL == 'https':
            mirage_prefix = 'docker2'

        self.MIRAGE_API_URL = self._url(mirage_prefix)

    def _url(self, subdomain):
        return self.PROTOCOL + '://' + subdomain + self.REVERSE_PROXY_SUFFIX

    def list(self):
        list_url = urljoin(self.MIRAGE_API_URL, "api/list")
        f = urlopen(list_url)
        return f.read()

    def launch(self, subdomain, branch, image):
        data = dict(
            subdomain=subdomain,
            branch=branch,
            image=image
        )

        launch_url = urljoin(self.MIRAGE_API_URL, "api/launch")
        f = urlopen(launch_url, urlencode(data))

        return f.read()

    def active_url_list(self):
        subdomains = self.list()
        active_list = self.active_subdomain_list(subdomains)

        return [self._url(subdomain) for subdomain in active_list]

    def active_subdomain_list(self, datas):
        jsons = json.loads(datas)
        subdomains = []
        for domain in jsons['result']:
            subdomains.append(domain['subdomain'])

        return subdomains

if __name__ == '__main__':
    app.run(host='0.0.0.0')
